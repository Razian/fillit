/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tetris.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 18:50:46 by tchivert          #+#    #+#             */
/*   Updated: 2019/05/24 21:23:58 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_tetris	ft_tbuild(char letter, t_block *block)
{
	t_tetris	tetri;
	int			i;

	i = 0;
	tetri.letter = letter;
	while (i < 4)
	{
		tetri.block[i] = block[i];
		i++;
	}
	return (tetri);
}

t_block		ft_coords(int x, int y)
{
	t_block	block;

	block.x = x;
	block.y = y;
	return (block);
}

t_tetris	*ft_create_tetris(char **split, t_file *p)
{
	t_block		block[4];
	t_tetris	*tetri;

	if (!(tetri = (t_tetris *)malloc(sizeof(t_tetris) * (p->nb_pieces + 1))))
		return (NULL);
	p->i = 0;
	p->nb_pieces = 0;
	while (split[p->i])
	{
		p->j = 0;
		p->k = 0;
		while (split[p->i][p->j])
		{
			if (split[p->i][p->j] == '#')
			{
				if (p->i == 0)
					block[p->k++] = ft_coords((p->j / 5), (p->j % 5));
				else
					block[p->k++] = ft_coords((p->j / 5), ((p->j % 5) - 1));
			}
			p->j++;
		}
		tetri[p->nb_pieces++] = ft_tbuild('A' + p->i++, block);
	}
	return (tetri);
}

t_tetris	*ft_make_relative(t_tetris *tetri, t_file *p)
{
	int	i;
	int	j;
	int	first_sharp_x;
	int	first_sharp_y;

	i = 0;
	while (i < p->nb_pieces)
	{
		first_sharp_x = tetri[i].block[0].x;
		first_sharp_y = tetri[i].block[0].y;
		j = 0;
		while (j < 4)
		{
			tetri[i].block[j].x = tetri[i].block[j].x - first_sharp_x;
			tetri[i].block[j].y = tetri[i].block[j].y - first_sharp_y;
			j++;
		}
		i++;
	}
	return (tetri);
}
