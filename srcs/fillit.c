/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 13:16:38 by tchivert          #+#    #+#             */
/*   Updated: 2019/05/29 15:20:14 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_read_file(int fd, char **file)
{
	int			ret;
	char		buff[2];
	char		*str;
	char		*tmp;

	if ((str = ft_strnew(1)) == NULL)
		return (-1);
	while ((ret = read(fd, buff, 1) > 0))
	{
		buff[ret] = '\0';
		tmp = str;
		str = ft_strjoin(str, buff);
		ft_strdel(&tmp);
	}
	if (str == NULL)
		return (-1);
	*file = str;
	return (ret);
}

void	ft_free_all(char **file, char **split, t_file *props, t_tetris *tetris)
{
	int			i;

	free(*file);
	i = 0;
	while (split[i])
	{
		ft_strdel(&split[i]);
		i++;
	}
	free(split);
	free(tetris);
	free(props);
}

int		main(int ac, char **av)
{
	char		*file;
	char		**split;
	t_file		*props;
	t_tetris	*tetriminos;

	if (ac == 2)
	{
		if ((ft_read_file(open(av[1], O_RDONLY), &file)) == -1
				|| ((props = ft_fprops(&file)) == NULL)
				|| ((split = ft_tsplit(&file, props)) == NULL)
				|| (ft_tcheck(split, props) == -1)
				|| ((tetriminos = ft_create_tetris(split, props)) == NULL)
				|| ((tetriminos = ft_make_relative(tetriminos, props)) == NULL))
		{
			ft_putendl("error");
			return (-1);
		}
		ft_solve(tetriminos, props);
		ft_free_all(&file, split, props, tetriminos);
	}
	else
		ft_putendl("usage: ./fillit [filename]");
	return (0);
}
