/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 21:45:39 by tchivert          #+#    #+#             */
/*   Updated: 2019/05/30 16:32:34 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_check_available(char **map, t_tetris tetris, int map_size,
		int pos)
{
	int			i;
	t_block		current;
	t_block		next;

	current.x = (pos / map_size);
	current.y = (pos % map_size);
	i = 0;
	while (i < 4)
	{
		next.x = (tetris.block[i].x + current.x);
		next.y = (tetris.block[i].y + current.y);
		if (next.x >= map_size || next.y >= map_size || next.x < 0
				|| next.y < 0 || map[next.x][next.y] != '.')
			return (0);
		i++;
	}
	return (1);
}

void	ft_put_tetris(char **map, t_tetris tetris, int map_size,
		int pos)
{
	int			i;
	t_block		current;
	t_block		next;

	current.x = (pos / map_size);
	current.y = (pos % map_size);
	i = 0;
	while (i < 4)
	{
		next.x = (tetris.block[i].x + current.x);
		next.y = (tetris.block[i].y + current.y);
		map[next.x][next.y] = tetris.letter;
		i++;
	}
}

void	ft_unput_tetris(char **map, t_tetris tetris, int map_size,
		int pos)
{
	int			i;
	t_block		current;
	t_block		next;

	current.x = (pos / map_size);
	current.y = (pos % map_size);
	i = 0;
	while (i < 4)
	{
		next.x = (tetris.block[i].x + current.x);
		next.y = (tetris.block[i].y + current.y);
		map[next.x][next.y] = '.';
		i++;
	}
}

int		ft_place_recursive(char **map, t_tetris *tetris, t_file *p,
		int nb_tetris)
{
	int		i;

	if (nb_tetris == p->nb_pieces)
		return (1);
	i = 0;
	while (i < (p->size * p->size))
	{
		if (ft_check_available(map, tetris[nb_tetris], p->size, i))
		{
			ft_put_tetris(map, tetris[nb_tetris], p->size, i);
			if (ft_place_recursive(map, tetris, p, nb_tetris + 1))
				return (1);
			ft_unput_tetris(map, tetris[nb_tetris], p->size, i);
		}
		i++;
	}
	return (0);
}

void	ft_solve(t_tetris *tetris, t_file *p)
{
	char	**map;

	p->size = ft_calcul_min_map(p->nb_pieces * 4);
	map = ft_create_map(p->size);
	while (ft_place_recursive(map, tetris, p, 0) == 0)
	{
		ft_map_destruct(map, p->size);
		free(map);
		p->size++;
		map = ft_create_map(p->size);
	}
	ft_print_map(map, p->size);
	ft_map_destruct(map, p->size);
	free(map);
}
