/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/21 18:35:53 by tchivert          #+#    #+#             */
/*   Updated: 2019/05/28 21:53:54 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include "../libft/libft.h"
# include <stdlib.h>
# include <string.h>
# include <fcntl.h>
# include <unistd.h>

typedef struct		s_file
{
	int				i;
	int				j;
	int				k;
	int				nb_pts;
	int				nb_sharps;
	int				nb_endl;
	int				nb_pieces;
	int				nb_link;
	int				size;
}					t_file;

typedef struct		s_block
{
	int				x;
	int				y;
}					t_block;

typedef struct		s_tetris
{
	char			letter;
	struct s_block	block[4];
}					t_tetris;

int					ft_read_file(int fd, char **file);
int					ft_tcheck(char **split, t_file *props);
char				**ft_tsplit(char **file, t_file *props);
t_file				*ft_fprops(char **file);
t_tetris			*ft_create_tetris(char **split, t_file *p);
t_tetris			*ft_make_relative(t_tetris *tetri, t_file *p);
void				ft_solve(t_tetris *tetris, t_file *p);
char				**ft_create_map(int map_size);
int					ft_calcul_min_map(int nb);
void				ft_map_destruct(char **map, int map_size);
void				ft_print_map(char **array, int map_size);

#endif
