/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fprops.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 17:59:06 by tchivert          #+#    #+#             */
/*   Updated: 2019/05/30 15:39:14 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_file	*ft_pbuild(void)
{
	t_file	*props;

	if (!(props = (t_file *)malloc(sizeof(t_file))))
		return (NULL);
	props->i = 0;
	props->j = 0;
	props->k = 0;
	props->nb_pts = 0;
	props->nb_sharps = 0;
	props->nb_endl = 0;
	props->nb_pieces = 0;
	props->nb_link = 0;
	props->size = 0;
	return (props);
}

int		ft_fcheck(t_file *props, char **file)
{
	int	len;
	int	endl;

	len = 0;
	endl = 0;
	while ((*file)[len])
	{
		if ((*file)[len] == '.')
			props->nb_pts++;
		if ((*file)[len] == '\n')
			endl = 0;
		if (endl++ >= 5)
			return (-1);
		len++;
	}
	if (props->nb_sharps % 4 != 0
			|| (props->nb_endl + 1) / 5 != props->nb_pieces)
		return (-1);
	if ((*file)[len - 1] == '\n' && (*file)[len - 2] == '\n')
		return (-1);
	if (props->nb_pieces == 0 || props->nb_pieces > 26)
		return (-1);
	if (props->nb_pts != props->nb_pieces * 12)
		return (-1);
	return (0);
}

t_file	*ft_fprops(char **file)
{
	static int	i = 0;
	static int	j = 0;
	t_file		*props;

	props = ft_pbuild();
	while ((*file)[i])
	{
		if ((*file)[i] != '\n' && (*file)[i] != '.' && (*file)[i] != '#')
			return (NULL);
		if ((*file)[i] == '\n')
		{
			if (j == 1 && props->nb_endl % 5 != 4)
				return (NULL);
			j = 0;
			props->nb_endl++;
		}
		if ((*file)[i] == '#')
			props->nb_sharps++;
		i++;
		j++;
	}
	props->nb_pieces = (props->nb_sharps / 4);
	if (ft_fcheck(props, file) == -1)
		return (NULL);
	return (props);
}
