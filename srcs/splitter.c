/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   splitter.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 17:55:46 by tchivert          #+#    #+#             */
/*   Updated: 2019/05/28 21:58:40 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			ft_is_piece(char *str)
{
	if (*(str) == '\n' && *(str + 1) == '\n')
		return (1);
	return (0);
}

int			ft_count_chars(char *str)
{
	int		i;

	i = 0;
	while (str[i] && !ft_is_piece(&str[i]))
		i++;
	return (i);
}

int			ft_tcheck(char **split, t_file *p)
{
	while (split[p->i])
	{
		p->nb_link = 0;
		p->j = 0;
		while (split[p->i][p->j])
		{
			if (split[p->i][p->j] == '#')
			{
				if (p->j - 5 >= 0 && split[p->i][p->j - 5] == '#')
					p->nb_link++;
				if (p->j + 5 < 20 && split[p->i][p->j + 5] == '#')
					p->nb_link++;
				if (p->j - 1 >= 0 && split[p->i][p->j - 1] == '#')
					p->nb_link++;
				if (p->j + 1 < 20 && split[p->i][p->j + 1] == '#')
					p->nb_link++;
			}
			p->j++;
		}
		if ((p->nb_link % 6 != 0 && (p->nb_link % 8 != 0)) || p->nb_link == 0)
			return (-1);
		p->i++;
	}
	return (0);
}

char		**ft_tsplit(char **file, t_file *p)
{
	int		i;
	int		j;
	char	**split;
	int		piece_count;
	int		len;

	if (!(split = (char**)malloc(sizeof(char*) * (p->nb_pieces + 1))))
		return (NULL);
	i = 0;
	piece_count = 0;
	while (piece_count < p->nb_pieces)
	{
		while ((*file)[i] && ft_is_piece(&((*file)[i])))
			i++;
		len = ft_count_chars(&((*file)[i]));
		if (!(split[piece_count] = (char*)malloc(sizeof(char) * (len + 1))))
			return (NULL);
		j = 0;
		while (j < len)
			split[piece_count][j++] = (*file)[i++];
		split[piece_count][j] = '\0';
		piece_count++;
	}
	split[piece_count] = 0;
	return (split);
}
