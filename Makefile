# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tchivert <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/05/29 15:13:43 by tchivert          #+#    #+#              #
#    Updated: 2019/05/29 15:44:02 by tchivert         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fillit
SRC = srcs/fillit.c \
	  srcs/fprops.c \
	  srcs/splitter.c \
	  srcs/tetris.c \
	  srcs/solve.c \
	  srcs/map.c
LIBFT = libft/libft.a
OBJ = $(SRC:.c=.o)
INCLUDES = ./
CC = clang
CFLAGS = -Werror -Wall -Wextra

all: $(NAME)

$(NAME): $(LIBFT) $(SRC)
	$(CC) $(CFLAGS) $(SRC) $(LIBFT) -o $(NAME)

$(LIBFT):
	make -C libft fclean
	make -C libft

clean:
	/bin/rm -f $(OBJ)
	make -C libft clean

fclean: clean
	/bin/rm -f $(NAME)
	make -C libft fclean

re: fclean all
